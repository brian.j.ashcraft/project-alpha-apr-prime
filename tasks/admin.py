from django.contrib import admin
from tasks.models import Task
# Register your models here.
@admin.register(Task)
class ModelnameAdmin(admin.ModelAdmin):
    task = ("start_date", "due_date", "is_completed", "project", "assignee")
